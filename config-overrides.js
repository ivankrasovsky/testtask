const { override, fixBabelImports, addLessLoader } = require('customize-cra');
module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      '@layout-header-background': '#f2f2f2',
      '@layout-body-background': '#ffffff',
      '@layout-sider-background': '#d7d7d7',
      '@layout-footer-background': '@layout-header-background'
    }
  })
);