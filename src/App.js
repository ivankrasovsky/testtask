import React from 'react';
import { Layout as AntdLayout } from 'antd';
import Layout from './layout'
import FormWrapper from 'components/FormWrapper';
import './App.css';

const { Sider, Content } = AntdLayout;

function App() {
  return (
    <Layout>
      <Sider>Left side menu</Sider>
      <Content>
        <FormWrapper />
      </Content>
    </Layout>
  );
}

export default App;
