import React from 'react';
import { Icon, Row, Col } from 'antd';
import { textRight } from 'components/Drawer/Line/line.module.css';
import { list_item } from '../index.module.css';

const Line = ({ line, drawerShow }) => (
  <Row className={list_item} onClick={() => drawerShow(line)}>
    <Col span={12}>
      <p>{line.title}</p>
    </Col>
    <Col span={12} className={textRight}>
      <Icon type="caret-right" />
    </Col>
  </Row>
);

export default Line;