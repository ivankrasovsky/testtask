import React, { useState } from 'react';
import * as styles from './index.module.css';
import Drawer from 'components/Drawer';
import Line from './Line';


const FormWrapper = () => {
  const [drawerItem, setDrawerItem] = useState(null);

  const [items, setItems] = useState([
    {
      title: 'Имя, телефон, email',
      items: [
        {
          title: 'Имя фамилия',
          titleEdit: 'Изменить имя и фамилию',
          children: [
            {
              type: 'input',
              name: 'firstName',
              placeholder: 'Имя',
              value: 'Ivan'
            },
            {
              type: 'input',
              name: 'lastName',
              placeholder: 'Фамилия',
              value: 'Ivanov'
            }
          ]
        },
        {
          title: 'Email',
          titleEdit: 'Изменить email',
          children: [
            {
              type: 'input',
              name: 'email',
              placeholder: 'email',
              value: 'ivanov@gmail.com'
            },
            {
              type: 'text',
              value: 'После изменения email его нужно будет верифицировать'
            }
          ]
        },
        {
          title: 'Телефон',
          titleEdit: 'Изменить телефон',
          children: [
            {
              type: 'input',
              name: 'phone',
              placeholder: 'Телефон',
              value: '0670000000'
            }
          ]
        }
      ]
    },
    {
      title: 'Страна, часовой пояс, язык',
      items: [
        {
          title: 'Страна',
          titleEdit: 'Изменить страну',
          children: [
            {
              type: 'input',
              name: 'country',
              placeholder: 'Страна',
              value: 'Ukraine'
            }
          ]
        },
        {
          title: 'Часовой пояс',
          titleEdit: 'Изменить часовой пояс',
          children: [
            {
              type: 'input',
              name: 'time',
              placeholder: 'Часовой пояс',
              value: '-2'
            }
          ]
        }
      ]
    }
  ]);

  const drawerShow = (item) => {
    setDrawerItem(item)
  }

  const updateFromDrawer = (data) => {
    setItems(items.map(item => {
      return item.title === drawerItem.title ? Object.assign(item, { items: data }) : item;
    }))
    drawerHide()
  }

  const drawerHide = () => {
    setDrawerItem(null)
  }

  return (
    <div className={styles.wrapper} id='drawer'>
      <div className={styles.container}  >
        <div className={styles.header}>
          <div className={styles.header_item}>
            <div>{`${items[0].items[0].children[0].value} ${items[0].items[0].children[1].value}`}</div>
            <div>{items[0].items[1].children[0].value}</div>
          </div>
          <div className={styles.header_item}>
            <div>{`ID 28366`}</div>
            <div>{`Демо режим`}</div>
          </div>
        </div>
        <div className={styles.list}>
          {items.map((line, i) => 
            <Line 
              key={i} 
              line={line} 
              drawerShow={drawerShow}/>
          )}
        </div>
        {drawerItem !== null && <Drawer
          visible={true}
          title={drawerItem.title}
          titleEdit={drawerItem.titleEdit}
          items={drawerItem.items}
          handlerSave={updateFromDrawer}
          handlerClose={drawerHide}
        />}
      </div>
    </div>
  )
}

export default FormWrapper;