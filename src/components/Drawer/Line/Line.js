import React, { Fragment, useState } from 'react';
import { Field } from 'formik';
import filter from 'lodash/filter';
import { Icon, Row, Col, Input, Typography } from 'antd';
import * as styles from './line.module.css';

const Line = ({ line }) => {

  const [open, setOpen] = useState(false);
  const toogle = () => {
    setOpen(!open);
  }
  const { Text } = Typography;

  return (
    <Fragment>
      <Row className={styles.lineFirstLvl} onClick={toogle}>
        <Col span={12}>
          <p className={styles.p}>{open ? line.titleEdit : line.title} </p>
        </Col>
        <Col span={12} className={styles.textRight}>
          <Icon type={open ? 'caret-up' : 'caret-down'} />
        </Col>
      </Row>
      {open && line.children.map((itm, idx) => {
        if (itm.type === 'input') return <Row key={idx}><Col><Field render=
          {({ field }) => (
            <Input {...field} />
          )}
          name={itm.name} placeholder={itm.name} /></Col></Row>;
        return <Row key={idx}><Col><p>{itm.value}</p></Col></Row>;
      })}
      {!open && <Text onClick={toogle} className={styles.margin}>{filter(line.children, { type: 'input' }).map(item => item.value).join(' ')}</Text>}
    </Fragment>
  )
}

export default Line;