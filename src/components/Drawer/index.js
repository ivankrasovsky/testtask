import React from 'react';
import { Formik } from 'formik';
import * as styles from './index.module.css';
import { textRight } from './Line/line.module.css';
import { reduce, filter } from 'lodash';
import Line from './Line/Line';
import { Button, Drawer as AntdDrawer, Row, Col, Typography } from 'antd';

function Drawer({ visible, title, items, handlerClose, handlerSave }) {
  const { Title } = Typography;

  return (
    <AntdDrawer
      onClose={handlerClose}
      visible={visible}
      closable={false}
      getContainer='#drawer'
      width={500}
      style={{ position: 'relative', marginTop: 10 }}
    >
      <Formik
        initialValues={reduce(items, (hash, item) => {
          filter(item.children, { type: 'input' }).map(itm => {
            hash = { ...hash, [itm.name]: itm.value }
            return hash;
          })
          return hash;
        }, {})}
        onSubmit={(values) => {
          handlerSave(
            items.map(item => {
              return Object.assign({
                ...item,
                children: item.children.map(itm => Object.assign(itm, itm.type === 'input' ? { value: values[itm.name] } : {}))
              })
            })
          )
        }}
      >
        {({
          values,
          handleSubmit
        }) => (
            <form onSubmit={handleSubmit} className={styles.form}>
              <Row>
                <Col span={12}><Title level={4}>{title}</Title></Col>
                <Col span={12} className={textRight}><Button type='primary' onClick={() => handleSubmit(values)}>Сохранить</Button></Col>
              </Row>
              {items.map((item, index) => <Line key={index} line={item} />)}
            </form>
          )}
      </Formik>
    </AntdDrawer >
  );
}

export default Drawer;