import React from 'react';
import { Layout as AntdLayout } from 'antd';
import Header from 'components/Header';
import Footer from 'components/Footer';

function Layout({ children }) {
  return (
    <AntdLayout>
      <Header />
      <AntdLayout>
        {children}
      </AntdLayout>
      <Footer />
      </AntdLayout>
      );
    }

export default Layout;

